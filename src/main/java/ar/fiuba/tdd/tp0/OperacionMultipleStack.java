package ar.fiuba.tdd.tp0;

import java.util.*;

class IteraradorOperacionStack {
	public static void iterarOperacionStack(LinkedList<Float> stack, OperacionStack op) {
		int cantOperandos = stack.size() - 1;
		for (int i = 0; i < cantOperandos; i++) {
			op.calcular(stack);
		}
	}
}

class ProductoMultipleStack implements OperacionStack {
	public void calcular(LinkedList<Float> stack) {
		OperacionStack productoStack = new ProductoStack();
		IteraradorOperacionStack.iterarOperacionStack(stack, productoStack);
	}
}

class DivisionMultipleStack implements OperacionStack {
	public void calcular(LinkedList<Float> stack) {
		OperacionStack divisionInversaStack = new DivisionInversaStack();
		IteraradorOperacionStack.iterarOperacionStack(stack, divisionInversaStack);
	}
}

class SumaMultipleStack implements OperacionStack {
	public void calcular(LinkedList<Float> stack) {
		OperacionStack sumaStack = new SumaStack();
		IteraradorOperacionStack.iterarOperacionStack(stack, sumaStack);
	}
}

class RestaMultipleStack implements OperacionStack {
	public void calcular(LinkedList<Float> stack) {
		OperacionStack restaInversaStack = new RestaInversaStack();
		IteraradorOperacionStack.iterarOperacionStack(stack, restaInversaStack);
	}
}

/*
 * otra forma de implementar la resta multiple class RestaMultipleStack
 * implements OperacionStack { public void calcular(LinkedList<Float> stack) {
 * int cantOperandos = stack.size() - 1; float resultadoResta = 0; while
 * (cantOperandos > 0) { resultadoResta = resultadoResta - stack.pop();
 * cantOperandos--; } stack.push(resultadoResta + stack.pop()); } }
 */