package ar.fiuba.tdd.tp0;

import java.util.*;

interface OperacionStack {
	void calcular(LinkedList<Float> stack);
}

class SumaStack implements OperacionStack {
	public void calcular(LinkedList<Float> stack) {
		Optional.of(stack.size()).filter(i -> i >= 2).orElseThrow(() -> new IllegalArgumentException());
		float operando2 = stack.pop();
		float operando1 = stack.pop();
		stack.push(operando1 + operando2);
	}
}

class ProductoStack implements OperacionStack {
	public void calcular(LinkedList<Float> stack) {
		float operando2 = stack.pop();
		float operando1 = stack.pop();
		stack.push(operando1 * operando2);
	}
}

class DivisionStack implements OperacionStack {
	public void calcular(LinkedList<Float> stack) {
		float operando2 = stack.pop();
		float operando1 = stack.pop();
		stack.push(operando1 / operando2);
	}
}

class RestaStack implements OperacionStack {
	public void calcular(LinkedList<Float> stack) {
		float operando2 = stack.pop();
		float operando1 = stack.pop();
		stack.push(operando1 - operando2);
	}
}

class RestaInversaStack implements OperacionStack {
	public void calcular(LinkedList<Float> stack) {
		stack.push(stack.pop() - stack.pop());
	}
}

class DivisionInversaStack implements OperacionStack {
	public void calcular(LinkedList<Float> stack) {
		stack.push(stack.pop() / stack.pop());
	}
}

class ModStack implements OperacionStack {
	public void calcular(LinkedList<Float> stack) {
		float secondOperand = stack.pop();
		float firstOperand = stack.pop();
		stack.push(firstOperand % secondOperand);
	}
}

class EmptyStack implements OperacionStack {
	public void calcular(LinkedList<Float> stack) {
		throw new IllegalArgumentException();
	}
}
