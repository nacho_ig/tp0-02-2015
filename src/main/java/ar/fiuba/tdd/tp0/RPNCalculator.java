package ar.fiuba.tdd.tp0;

import java.util.*;

public class RPNCalculator {

	public float eval(String expr) {
		// Lanzar excepcion si expresion=null
		Optional.ofNullable(expr).orElseThrow(() -> new IllegalArgumentException());

		Map<String, OperacionStack> map = new HashMap<String, OperacionStack>();

		map.put("+", new SumaStack());
		map.put("-", new RestaStack());
		map.put("", new EmptyStack());
		map.put("/", new DivisionStack());
		map.put("*", new ProductoStack());
		map.put("MOD", new ModStack());
		map.put("**", new ProductoMultipleStack());
		map.put("//", new DivisionMultipleStack());
		map.put("++", new SumaMultipleStack());
		map.put("--", new RestaMultipleStack());

		LinkedList<Float> stack = new LinkedList<Float>();

		for (String token : expr.split("\\s")) {

			Float tokenNum = null;

			try {
				tokenNum = Float.parseFloat(token);
			} catch (NumberFormatException e) {
			}
			if (tokenNum != null) {
				stack.push(Float.parseFloat(token + ""));
			} else {
				// Lanzar excepcion si la expresion es incompleta
				Optional.of(stack.size()).filter(i -> i >= 1).orElseThrow(() -> new IllegalArgumentException());

				// Lanzar exepcion si el operador no es valido
				Optional.ofNullable(map.get(token)).orElseThrow(() -> new IllegalArgumentException());

				map.get(token).calcular(stack);
			}
		}
		return stack.pop();
	}
}